# README #

----

## Requisitos ##

* NODE/NPM (https://nodejs.org/en/)
* BOWER (https://bower.io)
* Gulp ^3.9.1 (https://gulpjs.com/) 
* SASS (https://sass-lang.com/)

----

## Start ##

* `$ npm install`
* `$ bower install`
* `$ gulp serve`

----

## Como usar? ##

### PUG ###
* `app/templates`
* Base HTML em PUG. O Gulp faz a conversão para HTML enquanto estiver rodando o server local e também quando gerar a Build.

### SASS ###
* `app/source/sass/styles`
* Base de estilos que utiliza a melhor synstax do mundo todo SASS

### Imagens ###
* `app/source/img`

### Favicons ###
* `app/source/favicon`

### Icones ###
* `app/source/icon/source`

----

## Build ##

* `$ gulp build`
* Na raiz do projeto vai ser gerado a Build com o código HTML + SASS e Javascript minificados.

----

## Deploy ##
Para deploy simples, pode ser utilizado a ferramentas como Surge ou Now:

Utilizando o Surge (https://surge.sh/)

* `$ npm install --global surge`
* `$ surge`
* Create Account or Sign In
* `$ surge build/`
* Especificar o nome do domínio

